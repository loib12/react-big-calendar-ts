import format from "date-fns/format";
import getDay from "date-fns/getDay";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import React, { useState } from "react";
import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import "react-datepicker/dist/react-datepicker.css";
import "./App.css";
import DialogAdd from "./components/events/DialogAdd";
import { EVENTS } from "./constants";
import useToggle from "./components/events/hooks/useToggle";

const locales = {
  "en-US": require("date-fns/locale/en-US"),
};

const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

const initialState = {
  title: "Vacation",
  start: new Date(2021, 10, 7),
  end: new Date(2021, 10, 8),
};

interface IAllEvent {
  title: string;
  start: Date;
  end: Date;
}

function App() {
  const [newEvent, setNewEvent] = useState<IAllEvent>(initialState);
  const [allEvents, setAllEvents] = useState<IAllEvent[]>(EVENTS);
  const [isOpen, setIsOpen] = useToggle(false);

  function handleAddEvent() {
    setAllEvents([...allEvents, newEvent ]);
    setNewEvent(initialState);
    setIsOpen(false);
  }

  const handleChangeTitle = (event: Event) => {
    setNewEvent({ ...newEvent, title: ((event.target as HTMLTextAreaElement).value )});
  };

  const handleChangeStart = (start: Date) => {
    setNewEvent({ ...newEvent, start });
  };

  const handleChangeEnd = (end: Date) => {
    setNewEvent({ ...newEvent, end });
  };

  return (
    <div className="App">
      <h1>Calendar</h1>
      <h2>Add New Event</h2>
      <button onClick={() => setIsOpen(true)}> Add new event </button>
      <DialogAdd
        onAddEvent={handleAddEvent}
        onChangeTitle={handleChangeTitle}
        onChangeStart={handleChangeStart}
        onChangeEnd={handleChangeEnd}
        newEvent={newEvent}
        open={isOpen}
        close={() => setIsOpen(false)}
      />
      <Calendar
        localizer={localizer}
        events={allEvents}
        startAccessor="start"
        endAccessor="end"
        style={{ height: 500, margin: "50px" }}
      />
    </div>
  );
}

export default App;
