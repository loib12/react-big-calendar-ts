import React, { useState } from "react";
import DatePicker from "react-datepicker";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";

const DialogAdd = (props: any) => {
  const {
    onAddEvent,
    onChangeTitle,
    onChangeStart,
    onChangeEnd,
    newEvent,
    open,
    close,
  } = props;

  return (
    <Dialog
      open={open}
      onClose={close}
      style={{ width: "100%", height: "100%" }}
    >
      <TextField
        autoFocus
        margin="dense"
        label="Title Event"
        type="text"
        fullWidth
        variant="standard"
        value={newEvent.title}
        onChange={(e) => onChangeTitle(e)}
      />
      <DatePicker
        placeholderText="Start Date"
        selected={newEvent.start}
        onChange={(start: any) => onChangeStart(start)}
      />
      <DatePicker
        placeholderText="End Date"
        selected={newEvent.end}
        onChange={(end: any) => onChangeEnd(end)}
      />
      <Button style={{ marginTop: "10px" }} onClick={onAddEvent}>
        Add Event
      </Button>
    </Dialog>
  );
};

export default DialogAdd
