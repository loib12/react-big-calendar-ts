export const EVENTS = [
    {
      title: "Big Meeting",
      start: new Date(2021, 10, 2),
      end: new Date(2021, 10, 3),
    },
    {
      title: "Vacation",
      start: new Date(2021, 10, 7),
      end: new Date(2021, 10, 8),
    },
    {
      title: "Conference",
      start: new Date(2021, 10, 20),
      end: new Date(2021, 10, 21),
    },
  ];